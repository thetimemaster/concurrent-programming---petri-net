package alternator;

import petrinet.Transition;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class LetterWriter implements Runnable
{
    private char letter;
    private List<Transition<String>> inTransitions, outTransitions;

    LetterWriter(char letter, char[] allLetters)
    {
        inTransitions = new LinkedList<>();

        Map<String, Integer> inInput = new HashMap<>();

        List<String> inInhibitor = new LinkedList<>();

        for (char c : allLetters)
        {
            inInhibitor.add(c + "_InCritical");
        }

        inInhibitor.add(letter+"_WentLast");

        List<String> inReset = new LinkedList<>();

        for (char c : allLetters)
        {
            if (c != letter) inReset.add(c + "_WentLast");
        }

        Map<String, Integer> inOutput  = new HashMap<>();
        inOutput.put(letter+"_WentLast", 1);
        inOutput.put(letter+"_InCritical", 1);

        inTransitions.add(new Transition<>(
                inInput, inReset, inInhibitor, inOutput
        ));

        outTransitions = new LinkedList<>();

        Map<String, Integer> outInput = new HashMap<>();
        outInput.put(letter+"_InCritical", 1);
        List<String> outInhibitor = new LinkedList<>();
        List<String> outReset = new LinkedList<>();
        Map<String, Integer> outOutput  = new HashMap<>();

        outTransitions.add(new Transition<>(
                outInput, outReset, outInhibitor, outOutput
        ));

        this.letter = letter;
    }

    private void requestCritical() throws InterruptedException
    {
        Alternator.controller.fire(inTransitions);
    }

    private void freeCritical() throws InterruptedException
    {
        Alternator.controller.fire(outTransitions);
    }

    List<Transition<String>> getAllTransitions()
    {
        List<Transition<String>> list = new LinkedList<>(inTransitions);
        list.addAll(outTransitions);
        return list;
    }

    @Override
    public void run()
    {
        while(true)
        {
            if(Thread.currentThread().isInterrupted())
            {
                System.out.println(letter + " przerwany");
                break;
            }
            try
            {
                requestCritical();
                System.out.print(letter);
                System.out.print('.');
                freeCritical();
            }
            catch(InterruptedException exception)
            {
                System.out.println(letter + " przerwany");
                break;
            }
        }
    }
}