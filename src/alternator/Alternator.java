package alternator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;

public class Alternator
{
    static PetriNet<String> controller;

    public static void main(String[] args)
    {
        char[] letters = {'A', 'B', 'C'};
        Map<String, Integer> state = new HashMap<>();
        List<LetterWriter> writers = new LinkedList<>();
        List<Thread> threads = new LinkedList<>();
        List<Transition<String>> allTransitions = new LinkedList<>();

        for (char c : letters)
        {
            state.put(c + "_InCritical", 0);
            state.put(c + "_WentLast", 0);

            LetterWriter writer = new LetterWriter(c, letters);
            allTransitions.addAll(writer.getAllTransitions());
            writers.add(writer);
            threads.add(new Thread(writer, "Thread " + c));
        }

        controller = new PetriNet<>(state, false);

        Set<Map<String, Integer>> reachable = controller.reachable(allTransitions);

        System.out.println("Liczba osiągalnych stanów: " + reachable.size());
        for(Map<String, Integer> singleState : reachable)
        {
            int inCritical = 0;
            for(char c : letters)
            {
                inCritical += singleState.getOrDefault(c + "_InCritical", 0);
            }
            assert inCritical < 2;
        }

        for (Thread t : threads)
        {
            t.start();
        }

        try
        {
            Thread.sleep(30000);
        }
        catch(InterruptedException e)
        {
            System.out.print("Główny przerwany");
        }
        finally
        {
            for (Thread t : threads)
            {
                t.interrupt();
            }
        }
    }
}
