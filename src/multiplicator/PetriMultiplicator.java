package multiplicator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.Collection;

class PetriMultiplicator implements Runnable
{
    private Collection<Transition<String>> transitions;
    private PetriNet<String> net;
    private int fired;

    PetriMultiplicator(PetriNet<String> net)
    {
        fired = 0;
        this.net = net;
        transitions = Multiplicator.prepareTransitions();
    }

    @Override
    public void run()
    {
        try
        {
            while (true)
            {
                net.fire(transitions);
                fired++;
            }
        }
        catch (InterruptedException e)
        {
            System.out.println(Thread.currentThread().getName() + " przerwany, odpalił fire " + fired + " razy.");
        }
    }
}