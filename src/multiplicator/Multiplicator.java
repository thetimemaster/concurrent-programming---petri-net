package multiplicator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;

public class Multiplicator {

    private static final int NUMBER_SIZE = 31;
    private static final int THREAD_COUNT = 4;

    // Zwraca listę przejśc odpalanych przez wszystkie wątki
    static Collection<Transition<String> > prepareTransitions()
    {
        Collection<Transition<String >> transitions = new LinkedList<>();

        // Przejścia do podziału A i B na bity zapalone i niezapalone

        // Te przejścia przesuwają zapalony bit na pozycji k na pozycję k+1, jeśli wcześniej bit k+1
        // jest zgasony oraz z naszej liczby A lub B zostało co najmniej 2^k
        for (int bit = 1; bit < NUMBER_SIZE; bit++)
        {
            Map<String, Integer> inputA = new HashMap<>();
            inputA.put("num_A", 1 << Math.max(bit - 1, 0));
            inputA.put("A_bit_" + (bit - 1), 1);

            Map<String, Integer> outputA = new HashMap<>();
            outputA.put("A_bit_" + bit, 1);

            transitions.add(new Transition<>(
                    inputA, // input
                    Collections.emptyList(), // reset
                    Collections.singleton("A_bit_" + bit), // inhibitor
                    outputA
            ));


            Map<String, Integer> inputB = new HashMap<>();
            inputB.put("num_B", 1 << Math.max(bit - 1, 0));
            inputB.put("B_bit_" + (bit - 1), NUMBER_SIZE);

            transitions.add(new Transition<>(
                    inputB, // input
                    Collections.emptyList(), // reset
                    Collections.singleton("B_bit_" + bit), // inhibitor
                    Collections.singletonMap("B_bit_" + bit, NUMBER_SIZE) // output
            ));
        }

        // Te przejścia dodają do bitowej reprezntacji jeden gasząc prefiks zapalonych bitów i zapalając następny
        for (int bit = 0; bit < NUMBER_SIZE; bit++)
        {
            Map<String, Integer> inputA = new HashMap<>();
            inputA.put("num_A", 1);
            inputA.put("left_moves", bit * NUMBER_SIZE);
            for(int i = 0; i < bit; i++)
            {
                inputA.put("A_bit_" + i, 1);
            }

            Map<String, Integer> outputA = new HashMap<>();
            outputA.put("A_bit_" + bit, 1);
            outputA.put("left_moves", NUMBER_SIZE);

            transitions.add(new Transition<>(
               inputA,
               Collections.emptyList(),
               Collections.singleton("A_bit_" + bit),
               outputA
            ));

            Map<String, Integer> inputB = new HashMap<>();
            inputB.put("num_B", 1);
            for(int i = 0; i < bit; i++)
            {
                inputB.put("B_bit_" + i, NUMBER_SIZE);
            }

            transitions.add(new Transition<>(
                    inputB,
                    Collections.emptyList(),
                    Collections.singleton("B_bit_" + bit),
                    Collections.singletonMap("B_bit_" + bit, NUMBER_SIZE)
            ));
        }

        // Te przejścia przesuwają zapalone bity liczby A po kracie log x log i dodają odpowiednie potęgi 2 do wyniku
        for (int bit_a = 0; bit_a < NUMBER_SIZE; bit_a++)
        {
            for (int bit_b = 0; bit_b < NUMBER_SIZE; bit_b++)
            {
                // Jeśli bit bit_b był włączony w liczbie B
                Map<String, Integer> input_bit_in = new HashMap<>();
                input_bit_in.put("B_bit_" + bit_b, 1); // Zabieramy 1 z tokenów na tym bicie B
                input_bit_in.put("left_moves", 1); // Zabieramy 1 z ilości wszystkich ruchów przy mnożeniu

                if (bit_b == 0) input_bit_in.put("A_bit_" + bit_a, 1); // Jeśli to 0wy bit to zabieramy z reprezentacji A
                else input_bit_in.put("grid_" + bit_a + "_" + (bit_b - 1), 1); // Jeśli nie to z kratki wyżej


                Map<String, Integer> output_bit_in = new HashMap<>();
                output_bit_in.put("output", 1<<(bit_a + bit_b)); // Dodajemy do wyniku
                output_bit_in.put("grid_" + bit_a + "_" + bit_b, 1); // Dodajemy do tej kratki

                transitions.add(new Transition<>(
                        input_bit_in, // input
                        Collections.emptyList(), // reset
                        Collections.emptyList(), // inhibitor
                        output_bit_in // output
                ));


                // Jeśli bit bit_b nie był włączony w liczbie B
                Map<String, Integer> input_bit_out = new HashMap<>();
                input_bit_out.put("left_moves", 1); // Zabieramy 1 z ilości wszystkich ruchów przy mnożeniu

                if (bit_b == 0) input_bit_out.put("A_bit_"+bit_a, 1); // Jeśli to 0wy bit to zabieramy z reprezentacji A
                else input_bit_out.put("grid_" + bit_a + "_" + (bit_b - 1), 1); // Jeśli nie to z kratki wyżej

                transitions.add(new Transition<>(
                        input_bit_out, // input
                        Collections.emptyList(), // reset
                        Collections.singleton("B_bit_" + bit_b), // inhibitor, odpalamy to przejście tylko gdy nie było włącznego bitu w B
                        Collections.singletonMap("grid_" + bit_a + "_" + bit_b, 1) // output , dodajemy do kratki, do wyniku nie bo niezapalony w B
                ));
            }

        }

        return transitions;
    }

    // Zwraca listę zawierającą finalne przejście
    private static Transition<String> finalTransition()
    {
        Collection<String> inhibitors = new LinkedList<>();

        inhibitors.add("num_A");
        inhibitors.add("num_B");
        inhibitors.add("left_moves");

        return new Transition<>(
            Collections.emptyMap(),
            Collections.emptyList(),
            inhibitors,
            Collections.emptyMap()
        );
    }

    // Zwraca startowy stan sieci
    private static Map<String, Integer> basicState(int A, int B)
    {
        Map<String , Integer> state = new HashMap<>();

        state.put("num_A", A);
        state.put("num_B", B);

        return state;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();

        PetriNet<String> net = new PetriNet<>(basicState(A, B), true);

        List<Thread> threads = new LinkedList<>();

        for (int i = 0; i < THREAD_COUNT; i++)
        {
            Thread t = new Thread(new PetriMultiplicator(net));
            threads.add(t);
            t.start();
        }

        try
        {
            net.fire(Collections.singleton(finalTransition()));

            Set<Map<String, Integer> > output = net.reachable(Collections.emptyList()); // to będzie singleton

            for (Map<String, Integer> map : output)
            {
                System.out.println("Wynik: " + map.get("output"));
            }
        }
        catch (InterruptedException ignore)
        {
            System.err.println("Główny przerwany");
        }

        for (Thread t : threads)
        {
            t.interrupt();
        }
    }

}
