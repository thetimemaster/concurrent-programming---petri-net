package petrinet;

import java.util.*;
import java.util.concurrent.Semaphore;

public class PetriNet<T>
{

    private List<HaltedFireCall<T> > haltedFireCalls;
    private Semaphore mutex;
    private Map<T, Integer> state;

    /* Konstruktor PetriNet<T>(initial, fair) tworzy sieć Petriego z miejscami typu T, w której:
    - w stanie początkowym miejsce ma niezerową liczbę żetonów wtedy i tylko wtedy, gdy należy do
      initial.keySet(), przy czym initial.get(x) jest liczbą żetonów w miejscu x,
    - kolejność budzenia wątków oczekujących na wykonanie opisanej poniżej metody
      fire(transitions) jest określona parametrem fair.*/
    public PetriNet(Map<T, Integer> initial, boolean fair)
    {
        state = new HashMap<>(initial);
        mutex = new Semaphore(1, fair);
        haltedFireCalls = new LinkedList<>();
    }


    /* Metoda reachable(transitions) próbuje wyznaczyć zbiór wszystkich znakowań sieci,
    które są osiągalne (ang. reachable) z aktualnego jej stanu w rezultacie odpalenia, zero lub więcej razy,
    przejść z kolekcji transitions. Jeśli zbiór osiągalnych znakowań jest skończony, to jest on wynikiem metody.
    Mapa m, należąca do tego zbioru, reprezentuje znakowanie, w którym miejsce ma niezerową liczbę żetonów
    wtedy i tylko wtedy, gdy jest elementem m.keySet(), przy czym m.get(x) jest liczbą żetonów w miejscu x.
    Jeśli zbiór osiągalnych znakowań jest nieskończony, to wykonanie metody może się zapętlić lub zostać przerwane wyjątkiem.
    Można zauważyć, że wywołanie reachable(transitions) z pustą kolekcją przejść daje zbiór znakowań sieci,
    którego jedynym elementem jest znakowanie aktualne.*/
    public Set<Map<T, Integer>> reachable(Collection<Transition<T>> transitions)
    {
        Map<T,Integer> goBackTo = state; // Zapisujemy obecny stan do przywrócenia

        try
        {
            mutex.acquire();
            Set<Map<T, Integer> > reached = new HashSet<>();
            reached.add(state);

            Queue<Map<T,Integer> > toTest = new LinkedList<>();
            toTest.add(state);

            while (!toTest.isEmpty())
            {
                Map<T, Integer> top = toTest.remove();

                for (Transition<T> transition : transitions)
                {
                    state = new HashMap<>(top);
                    if (canFire(transition)) fireOne(transition);
                    if (!reached.contains(state))
                    {
                        reached.add(state);
                        toTest.add(state);
                    }
                }
            }
            state = goBackTo;
            mutex.release();
            return reached;
        }
        catch (InterruptedException e)
        {
            state = goBackTo;
            mutex.release();
            return null;
        }
    }


    /* Metoda fire(transitions) dostaje jako argument niepustą kolekcję przejść. Wstrzymuje wątek,
    jeśli żadne przejście z tej kolekcji nie jest dozwolone. Gdy w kolekcji są przejścia dozwolone, metoda odpala
    jedno z nich, dowolnie wybrane. Wynikiem metody jest odpalone przez nią przejście. Jeżeli sieć Petriego została
    utworzona konstruktorem z argumentem fair równym true, to spośród tych wątków wstrzymanych metodą fire(transitions),
    które w danej chwili można wznowić, wybierany jest wątek czekający najdłużej. W przypadku przerwania wątku,
    metoda fire(transitions) zgłasza wyjątek InterruptedException. */
    public Transition<T> fire(Collection<Transition<T>> transitions) throws InterruptedException
    {
        mutex.acquire();
        Transition<T> result = tryFire(transitions);

        if (result != null)
        {
            // Jeśli result != null to tryFire albo zwolnił mutex albo przekazał kontekst dalej do innego wątku,
            // w każym razie my nie musimy go zwalniać
            return result;
        }

        Semaphore wakeMeWhenReady = new Semaphore(0);
        HaltedFireCall<T> myCall = new HaltedFireCall<>(transitions, wakeMeWhenReady);

        haltedFireCalls.add(myCall);

        // Result nie był równy null więc zwalniamy mutex
        mutex.release();

        // Ten semafor jest zwalniany z innego, udanego tryFire, dostęp do mutexa jest z niego przekazany
        wakeMeWhenReady.acquire();
        haltedFireCalls.remove(myCall);
        Transition<T> done = tryFire(transitions);
        assert done != null; // Żeby wątek został wybrany do wybudzenia to musi istnieć dozwolone przejście

        return done;
    }


    /* Metoda findFireable(transitions) dostaje jako argument niepustą kolejkę przejść. Zwaca pierwsze dozwolone
    przejście w niej, lub null jeśli takie nie istnieje */
    private Transition<T> findFireable(Collection<Transition<T> > transitions)
    {
        for (Transition<T> transition : transitions)
        {
            if (canFire(transition)) return transition;
        }

        return null;
    }


    /* Metoda fireOne(transition) odpala dane przejście. Nie sprawdza czy było ono dozwolone. */
    private void fireOne(Transition<T> transition)
    {
        transition.input.forEach((place, tokens) ->
        {
            state.compute(place, (pos, present) ->
                    (present == null ? 0 : present) - tokens);

            if (state.get(place) == 0) state.remove(place);
        });

        transition.reset.forEach((place) -> state.remove(place));

        transition.output.forEach((place, tokens) ->
                state.compute(place, (pos, present) ->
                        (present == null ? 0 : present) + tokens));
    }

    /* Metoda tryFire(transitions) wynaga dostępu do mutexu i szuka, uzywając findFireable, dozwolonego przejścia P
    w kolekcji. Jeśli takie istnieje to je odpala, szuka czy istnieje wstrzymane fire które możę się odpalić, jeśli
    takie istnieje to je budzi i przekacuje mu mutex, jeśłi nie to je zwalnia.
    Zwraca P jeśli istnieje, jeśli nie, nie robi nic i zwraca null */
    private Transition<T> tryFire(Collection<Transition<T>> transitions)
    {
        // Zaczynamy metode z dostępem do mutexu
        Transition<T> found = findFireable(transitions);

        if (found != null)
        {
            fireOne(found);

            boolean foundNext = false;

            for (HaltedFireCall<T> halted: haltedFireCalls)
            {
                if (findFireable(halted.transitions) != null)
                {
                    // Znaleźliśmy jakiś wstrzymany fire, mutex przechodzi do niego
                    halted.wakeUpCall.release();
                    foundNext = true;
                    break;
                }
            }

            // Nie znaleźliśmy żadnego czekającego fire do przekazania kontekstu, zwalniamy mutex
            if (!foundNext)
            {
                mutex.release();
            }
        }

        return found;
    }

    /* Metoda canFire(transition) sprawdza, czy dane przejście jest dozwolone */
    private boolean canFire(Transition<T> transition)
    {
        boolean isOk = true;

        for (Map.Entry<T, Integer> one : transition.input.entrySet())
        {
            if (state.getOrDefault(one.getKey(), 0) < one.getValue()) isOk = false;
        }

        for (T place : transition.inhibitor)
        {
            if (state.getOrDefault(place, 0) != 0) isOk = false;
        }

        return isOk;
    }
}

