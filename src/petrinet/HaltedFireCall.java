package petrinet;

import java.util.Collection;
import java.util.concurrent.Semaphore;

class HaltedFireCall<T>
{
    Collection<Transition<T>> transitions;

    Semaphore wakeUpCall;

    HaltedFireCall(Collection<Transition<T>> transitions, Semaphore wakeUpCall)
    {
        this.wakeUpCall = wakeUpCall;
        this.transitions = transitions;
    }
}
