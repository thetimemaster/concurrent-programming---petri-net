package petrinet;

import java.util.Collection;
import java.util.Map;

public class Transition<T>
{
    // Sprawdzanie
    public Map<T, Integer> input;
    public Collection<T> inhibitor;

    // Output
    public Collection<T> reset;
    public Map<T, Integer> output;

    /* Konstruktor Transition<T>(input, reset, inhibitor, output) tworzy przejście między miejscami typu T. Przejście jest połączone:
    - krawędziami wejściowymi z miejscami należącymi do input.keySet(), przy czym wagą krawędzi prowadzącej z miejsca x jest input.get(x),
    - krawędziami zerującymi z miejscami należącymi do reset,
    - krawędziami wzbraniającymi z miejscami należącymi do inhibitor,
    - krawędziami wyjściowymi z miejscami należącymi do output.keySet(), przy czym wagą krawędzi prowadzącej do miejsca x jest output.get(x).*/
    public Transition(Map<T, Integer> input, Collection<T> reset, Collection<T> inhibitor, Map<T, Integer> output)
    {
        this.input = input;
        this.inhibitor = inhibitor;
        this.reset = reset;
        this.output = output;
    }
}
